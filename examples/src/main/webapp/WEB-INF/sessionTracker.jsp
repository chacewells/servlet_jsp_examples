<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>Session Tracker</title>
</head>
<body>
<div id="sessionInfo">
</div>
<c:forEach items="${attribsList}" var="attrib">
	<p>${attrib}</p>
</c:forEach>
<form method="post" action="tracker">
	<p>Name: <input type="text" name="name" /></p>
	<p>Value: <input type="text" name="value" /></p>
	<p><input type="submit" value="Submit" /></p>
</form>
</body>
</html>