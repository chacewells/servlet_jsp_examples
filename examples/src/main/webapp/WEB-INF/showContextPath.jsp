<%@ page language="java" contentType="text/html; charset=ISO-8859-15"
    pageEncoding="ISO-8859-15"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15">
<title>Insert title here</title>
</head>
<body>
application.getContextPath(): ${application.contextPath}<br />
request.getContextPath(): ${request.contextPath}<br />
pageContext.getRequest().getContextPath(): ${pageContext.request.contextPath}<br />
pageContext.getServletContext().getContextPath(): ${pageContext.servletContext.contextPath}<br />
config.getServletContext().getContextPath(): ${config.servletContext.contextPath}
</body>
</html>