<%@page import="java.text.DecimalFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<jsp:useBean id="pet" class="beans.Pet" scope="session" />
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Pet Properties</title>
</head>
<body>
<h2>Pet Properties</h2>
<p>Name: ${pet.name}</p>
<p>Species: ${pet.species}</p>
<p>Age: <%= new DecimalFormat("#,###").format(pet.getAge()) %></p>
<script type="text/javascript">
window.onload = function () {
};
</script>
</body>
</html>