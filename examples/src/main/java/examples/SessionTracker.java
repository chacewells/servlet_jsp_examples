package examples;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringEscapeUtils;

public class SessionTracker extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		String name = StringEscapeUtils.escapeHtml(request.getParameter("name"));
		String value = StringEscapeUtils.escapeHtml(request.getParameter("value"));
		
		if ( name != null && value != null ) {
			session.setAttribute(name, value);
		}
		
		Enumeration<String> sessionAttribs = session.getAttributeNames();
		List<Attribs> attribsList = new ArrayList<Attribs>();
		
		for (String aName, aVal; sessionAttribs.hasMoreElements();) {
			aName = sessionAttribs.nextElement();
			aVal = (String)session.getAttribute(aName);
			attribsList.add(new Attribs(aName, aVal));
		}
		
		request.setAttribute("attribsList", attribsList);
		getServletContext().getRequestDispatcher("/WEB-INF/sessionTracker.jsp").forward(request, response);
	}
	
	public class Attribs {
		String name;
		String val;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getVal() {
			return val;
		}
		public void setVal(String val) {
			this.val = val;
		}
		public Attribs(String name, String val) {
			super();
			this.name = name;
			this.val = val;
		}
		@Override
		public String toString() {
			return name + "=" + val;
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}
	
}
